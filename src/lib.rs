use std::{collections::HashMap, fmt};
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

const TIE_RESOLUTION_MAX_TRY: u8 = 5;

#[wasm_bindgen]
pub enum WasThereCompetition {
    Yes,
    No,
}

#[wasm_bindgen]
pub struct Universe {
    width: u32,
    height: u32,
    // We represent each cell directly by the player id of the owner
    // 0 := dead
    cells: Vec<u32>,
}

pub struct Neighbourhood {
    count: u8,
    claims: HashMap<u32, u16>,
}

// I was using this as a debugging tool :P
#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn console_log(words: &str) {
    alert(words);
}

#[wasm_bindgen]
impl Universe {
    pub fn tick(&mut self) {
        let mut next = self.cells.clone();

        for row in 0..self.height {
            for col in 0..self.width {
                let idx = self.get_index(row, col);
                let cell = self.cells[idx];
                let live_neighbours = self.live_neighbour_count(row, col, 1);

                let next_cell = match (cell, live_neighbours.count) {
                    (state, x) if (x < 2) && state > 0 => 0,
                    (state, 2) | (state, 3) if state > 0 => cell,
                    (state, x) if x > 3 && state > 0 => 0,
                    (0, 3) => {
                        match live_neighbours.claims.len() {
                            1 => match live_neighbours.claims.iter().nth(0) {
                                Some((i, _)) => *i,
                                None => 0,
                            },
                            2 => {
                                match live_neighbours
                                    .claims
                                    .iter()
                                    .max_by(|(_, n), (_, m)| n.cmp(m))
                                {
                                    Some((i, _)) => *i,
                                    None => 0,
                                }
                            }
                            _ => {
                                let mut ring_level = 2;
                                let mut claims =
                                    self.live_neighbour_count(row, col, ring_level).claims;
                                let mut highest_claimant = (0, 0);
                                while ring_level <= TIE_RESOLUTION_MAX_TRY {
                                    highest_claimant =
                                        match claims.iter().max_by(|(_, n), (_, m)| m.cmp(n)) {
                                            Some((&i, &n)) => (i, n),
                                            None => (0, 0),
                                        };
                                    // TODO: Remove this POS
                                    console_log(&format!(
                                        "claims: {:?}, {:?}",
                                        claims,
                                        match claims.iter().max_by(|(_, n), (_, m)| m.cmp(n)) {
                                            Some((&i, &n)) => (i, n),
                                            None => (0, 0),
                                        }
                                    ));
                                    if claims
                                        .iter()
                                        .filter(|(_, n)| **n == highest_claimant.1)
                                        .count()
                                        != 1
                                    {
                                        ring_level += 1;
                                        claims =
                                            self.live_neighbour_count(row, col, ring_level).claims;
                                    } else if highest_claimant == (0, 0) {
                                        continue;
                                    } else {
                                        break;
                                    }
                                }

                                if highest_claimant != (0, 0) {
                                    highest_claimant.0
                                } else {
                                    // This is when we just can't resolve with our special check.
                                    // We'll just get the oldest player and grant the target cell to
                                    // them
                                    match live_neighbours.claims.iter().min() {
                                        Some((i, _)) => *i,
                                        None => 0,
                                    }
                                }
                            }
                        }
                    }
                    // All cells that fall through the above shall remain themselves
                    (otherwise, _) => otherwise,
                };

                next[idx] = next_cell;
            }
        }

        self.cells = next;
    }

    fn get_index(&self, row: u32, column: u32) -> usize {
        (row * self.width + column) as usize
    }

    fn live_neighbour_count(&self, row: u32, column: u32, ring: u8) -> Neighbourhood {
        let mut count = 0;
        let mut claims: HashMap<u32, u16> = HashMap::new();
        // TODO: Check if this model of the ring is properly capturing the "ring"
        let ring_u32 = ring as u32;
        // We have to use self.height - 1 because we're using unsigned integers.
        // If we use -1, we'll get into trouble for when row = 0.
        for delta_row in [self.height - ring_u32, 0, ring_u32].iter().cloned() {
            for delta_col in [self.height - ring_u32, 0, ring_u32].iter().cloned() {
                // Skip if we're looking at the cell in concern
                if delta_row == 0 && delta_col == 0 {
                    continue;
                }

                // This modulo here takes care of the wrapping
                let neighbour_row = (row + delta_row) % self.height;
                let neighbour_col = (column + delta_col) % self.width;
                let idx = self.get_index(neighbour_row, neighbour_col);
                let cell = self.cells[idx];
                if cell > 0 {
                    let claimant = claims.entry(cell).or_insert(0);
                    *claimant += 1;
                    count += 1;
                }
            }
        }

        Neighbourhood { count, claims }
    }

    pub fn new(width: u32, height: u32, universe_snapshot: Vec<u32>) -> Universe {
        Universe {
            width,
            height,
            cells: universe_snapshot,
        }
    }

    pub fn render(&self) -> String {
        self.to_string()
    }
}

impl fmt::Display for Universe {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for line in self.cells.as_slice().chunks(self.width as usize) {
            for &cell in line {
                write!(f, "{}", cell)?;
            }
            write!(f, "\n")?;
        }

        Ok(())
    }
}
