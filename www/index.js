import { Universe } from "wasm-game-of-life";

const pre = document.getElementById("game-of-life-canvas");
const universe = Universe.new(
  6,
  6,
  [
    [0, 0, 0, 0, 0, 0],
    [0, 1, 0, 3, 0, 2],
    [0, 0, 2, 0, 0, 0],
    [0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
  ].flat()
);

const renderLoop = () => {
  pre.textContent = universe.render();
  universe.tick();

  setTimeout(() => {
    requestAnimationFrame(renderLoop);
  }, 500);
};

requestAnimationFrame(renderLoop);
